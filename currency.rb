require 'optparse'
require 'date'

options = {}
optparse = OptionParser.new do |opts|
  opts.on('-c', '--currencies CURRENCIES', 'Target currencies to retrieve the exchange, comma separated, e.g.: US,EUR') do |currencies|
    options[:currencies] = currencies.split(',')
  end

  opts.on('-d', '--date DATE', 'Historical date for exchange rates, defaults to today, format yyyy-mm-dd') do |date|
    options[:date] = Date.parse(date) rescue nil
  end

  opts.on('-b', '--best', 'Returns the exchange rate of the last seven days for the given currencies') do
    options[:best] = true
  end

  opts.on('-t', '--token ACCESS_KEY', 'Access token to access the currency service') do |token|
    options[:token] = token
  end

  opts.on('-h', '--help', 'prints the usage') do
    options[:usage] = true
  end
end

optparse.parse!

if options[:usage]
  puts optparse
  exit 0
end

if options.fetch(:currencies, []).empty?
  puts 'missing --currencies'
  puts optparse
  exit 1
end

unless options[:token]
  puts 'missing --token'
  puts optparse
  exit 1
end

require './lib/exchange/currency_layer'
require './lib/exchange'

currency_service = Exchange::CurrencyLayer.new(options[:token])
exchange = Exchange.new(
  target_currencies: options[:currencies],
  currency_service: currency_service
)
date = options.fetch(:date, Date.today)

if options.fetch(:best, false)
  require './lib/exchange/best_rate'

  currencies = options[:currencies]
  currency = currencies.first
  if currencies.size > 1
    puts "[WARN] Best rate does not support multiple currencies, retrieving for #{currency}"
  end

  best_rate = Exchange::BestRate.new(currency: currency, start_date: date, currency_service: currency_service)
  puts best_rate.find
else
  puts exchange.retrieve(at: date)
end
