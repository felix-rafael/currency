require 'spec_helper'
require 'date'
require './lib/exchange'

RSpec.describe Exchange do
  it 'retrieves the exchange for one currency' do
    currency_service = double('CurrencyService')
    exchange = described_class.new(
      target_currencies: ['EUR'],
      currency_service: currency_service
    )
    expect(currency_service).to receive(:historical).with(
      currencies: ['EUR'],
      at: Date.today
    ).and_return('EUR' => 0.812465)

    values = exchange.retrieve
    expect(values).to eq('EUR' => 0.812465)
  end

  it 'retrieves the exchange for multiple currencies' do
    currency_service = double('CurrencyService')
    exchange = described_class.new(
      target_currencies: ['EUR', 'CAD'],
      currency_service: currency_service
    )
    expect(currency_service).to receive(:historical).with(
      currencies: ['EUR', 'CAD'],
      at: Date.today
    ).and_return({
      'EUR' => 0.812465,
      'CAD' => 1.036362
    })

    values = exchange.retrieve
    expect(values).to eq({
      'EUR' => 0.812465,
      'CAD' => 1.036362
    })
  end

  it 'retrieves the exchange for currency and date' do
    currency_service = double('CurrencyService')
    exchange = described_class.new(
      target_currencies: ['EUR', 'CAD'],
      currency_service: currency_service
    )

    random_date = Date.new(2017, 1, 1)

    expect(currency_service).to receive(:historical).with(
      currencies: ['EUR', 'CAD'],
      at: random_date
    ).and_return({
      'EUR' => 0.812465,
      'CAD' => 1.036362
    })

    values = exchange.retrieve(at: random_date)
    expect(values).to eq({
      'EUR' => 0.812465,
      'CAD' => 1.036362
    })
  end
end
