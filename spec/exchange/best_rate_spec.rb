require 'spec_helper'

require './lib/exchange/best_rate'

RSpec.describe Exchange::BestRate do
  it 'queries for the last 7 days and find the best exchange rate' do
    currency_service = double('CurrencyService')

    start_date = Date.new(2017, 6, 7)
    best_rate = described_class.new(
      currency: 'EUR',
      start_date: start_date,
      time_frame: 3,
      currency_service: currency_service
    )

    expect(currency_service).to receive(:historical)
      .with(currencies: ['EUR'], at: start_date)
      .and_return('EUR' => 0.821343)
    expect(currency_service).to receive(:historical)
      .with(currencies: ['EUR'], at: Date.new(2017, 6, 6))
      .and_return('EUR' => 0.851342)
    expect(currency_service).to receive(:historical)
      .with(currencies: ['EUR'], at: Date.new(2017, 6, 5))
      .and_return('EUR' => 0.811342)
    expect(currency_service).to receive(:historical)
      .with(currencies: ['EUR'], at: Date.new(2017, 6, 4))
      .and_return('EUR' => 0.831342)

    expect(best_rate.find).to eq(date: Date.new(2017, 6, 6), exchange: 0.851342)
  end
end
