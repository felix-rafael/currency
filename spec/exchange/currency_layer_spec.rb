require 'spec_helper'

require './lib/exchange/currency_layer'

RSpec.describe Exchange::CurrencyLayer do
  describe 'retrieving historial data from currencies' do
    it 'queries the remote service and returns the response parsed' do
      http = double('Requester')
      currency_service = described_class.new('secret', 'USD', http)

      date = Date.today
      uri = URI("http://apilayer.net/api/historical?access_key=secret&date=#{date.strftime('%Y-%m-%d')}&currencies=EUR,CAD")
      expect(http).to receive(:get)
        .with(uri)
        .and_return(File.read('./spec/fixtures/historical.json'))

      currencies = currency_service.historical(currencies: ['EUR', 'CAD'], at: date)

      expect(currencies).to eq({
        'EUR' => 0.812465,
        'CAD' => 1.036362
      })
    end

    it 'raises exception when the request was unsuccessful' do
      http = double('Requester')
      currency_service = described_class.new('secret', 'USD', http)

      date = Date.today
      uri = URI("http://apilayer.net/api/historical?access_key=secret&date=#{date.strftime('%Y-%m-%d')}&currencies=EUR,CAD")
      expect(http).to receive(:get)
        .with(uri)
        .and_return(File.read('./spec/fixtures/historical_failed.json'))

      expect {
        currency_service.historical(currencies: ['EUR', 'CAD'], at: Date.today)
      }.to raise_error(Exchange::CurrencyLayer::InvalidRequest, '101 - You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com]')
    end
  end
end
