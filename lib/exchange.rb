class Exchange
  def initialize(target_currencies:, currency_service:)
    @target_currencies = target_currencies
    @currency_service = currency_service
  end

  def retrieve(at: Date.today)
    @currency_service.historical(
      currencies: @target_currencies,
      at: at
    )
  end
end
