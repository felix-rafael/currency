require 'net/http'
require 'uri'
require 'json'

class Exchange
  class CurrencyLayer
    InvalidRequest = Class.new(StandardError)
    DEFAULT_CURRENCY = 'USD'

    def initialize(access_token, source_currency = DEFAULT_CURRENCY, http_requester = Net::HTTP)
      @access_token = access_token
      @http_requester = http_requester
      @source_currency = source_currency
    end

    def historical(currencies:, at:)
      url = build_url(currencies: currencies, at: at)
      response = JSON(request(url))

      unless response['success']
        raise InvalidRequest, "#{response.dig('error', 'code')} - #{response.dig('error', 'info')}"
      end

      extract_currencies(response)
    end

    private

    def build_url(currencies:, at:)
      base_url = 'http://apilayer.net/api/historical'

      params = {
        access_key: @access_token,
        date: at.strftime('%Y-%m-%d'),
        currencies: currencies.join(','),
        # source: @source_currency # not supported in the free plan
      }.map { |k, v|  "#{k}=#{v}" }.join('&')

      URI("#{base_url}?#{params}")
    end

    def request(url)
      @http_requester.get(url)
    end

    def extract_currencies(json)
      json['quotes'].map do |currency, value|
        [currency.gsub(@source_currency, ''), value]
      end.to_h
    end
  end
end
