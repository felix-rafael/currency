class Exchange
  class BestRate
    DEFAULT_TIME_FRAME_IN_DAYS = 7

    def initialize(currency:, start_date: Date.today, time_frame: DEFAULT_TIME_FRAME_IN_DAYS, currency_service:)
      @currency = currency
      @start_date = start_date
      @time_frame = time_frame
      @currency_service = currency_service
    end

    def find
      best_date = @start_date
      best_exchange = 0

      (0..@time_frame).each do |day|
        current_date = @start_date - day
        currencies = @currency_service.historical(currencies: [@currency], at: current_date)

        exchange_rate = currencies[@currency]
        if exchange_rate > best_exchange
          best_exchange = exchange_rate
          best_date = current_date
        end
      end

      { date: best_date, exchange: best_exchange }
    end
  end
end
